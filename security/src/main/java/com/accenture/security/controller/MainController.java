package com.accenture.security.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.security.model.Empleado;
import com.accenture.security.model.Empresa;
import com.accenture.security.model.InputEmpleado;
import com.accenture.security.model.Usuario;
import com.accenture.security.repository.EmpleadoDao;
import com.accenture.security.repository.EmpresaDao;
import com.accenture.security.repository.UsuarioDao;


@Controller
public class MainController {
	
	@Autowired
	private EmpresaDao empresadao;
	
	@Autowired
	private EmpleadoDao empleadodao;
	
	@Autowired
	private UsuarioDao usuariodao;
	
//    @RequestMapping(value = "/username", method = RequestMethod.GET)
//    @ResponseBody
//    public String currentUserNameSimple(HttpServletRequest request) {
//        Principal principal = request.getUserPrincipal();
//        return principal.getName();
//    }
	
	@RequestMapping("/new")
	public String showNewPage(Model model) {
	    Empresa empresa = new Empresa();
	    model.addAttribute("empresa", empresa);
	     
	    return "newempresa";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveEmpresa(@ModelAttribute("empresa") Empresa empresa) {
	    empresadao.save(empresa);
	   
	    return "redirect:/";
	}
	
	// ==================================================================
	
	@RequestMapping(value = "/save2", method = RequestMethod.POST)
	public String saveEmpleado(@ModelAttribute("empleado") InputEmpleado empleado) {
		
		Empresa empr1 = empresadao.findById(empleado.getIdEmpresa()).orElse(null);
		
		Empleado emple1 = new Empleado();
		
			if(empr1 != null) {
			
			emple1.setEmpresa(empr1);
			emple1.setNombre(empleado.getEmpleado().getNombre());
			emple1.setEdad(empleado.getEmpleado().getEdad());
			emple1.setSueldo(empleado.getEmpleado().getSueldo());
			emple1.setAntiguedad(empleado.getEmpleado().getAntiguedad());
			emple1.setPatrimonio(empleado.getEmpleado().getAntiguedad(), empleado.getEmpleado().getSueldo());
			
			empleadodao.save(emple1);
			
			}
		
		return "redirect:/";
	}
	
	@RequestMapping("/new2")
	public String showNew2Page(Model model) {
		InputEmpleado empleado = new InputEmpleado();
		model.addAttribute("inputempleado", empleado);
		
		return "newempleado";
		}
	
	
    
	// ==================================================================
	
	
    @GetMapping(path= {"/username"})
    public String usuarioActual(HttpServletRequest request) {
    	Principal principal = request.getUserPrincipal();
    	return principal.getName();
    }
    
    //edito una empresa (es importante no tocar el ID ya que es GeneratedValue)
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") Long id) {
	    ModelAndView mav = new ModelAndView("Edit");
	    Empresa empresa = empresadao.getOne(id);
	    mav.addObject("empresa", empresa);
	     
	    return mav;
	}
	
    //muestro todas las empresas con modelview
	@GetMapping(path= {"/"})
	public ModelAndView verEmpresas(HttpServletRequest request) {
		
		Principal principal = request.getUserPrincipal();
		List <Empresa> empresasFound = empresadao.findAll();
		
		ModelAndView mav = new ModelAndView("empresas");
		
		mav.addObject("usuario", principal.getName());
		mav.addObject("empresas", empresasFound);
		
		return mav;
	}
	
	//registro un usuario para un futuro login
	@PostMapping(path = {"/registrar"})
	public Usuario registrar(@RequestBody Usuario usuario) {
		
		return usuariodao.save(usuario);
	}
	
	//doy de alta una empresa (cambio POST por GET para bypassear spring security)
	@GetMapping(path= {"/create"})
	public Empresa createEmpresa(@RequestBody Empresa empresa) {
		
		return empresadao.save(empresa);
	}
	
	
	
	//doy de alta un empleado, seteandole una empresa desde front (originalmente POST)
	@GetMapping(path= {"/create2"})
	public ResponseEntity<Object> createGallina(@RequestBody InputEmpleado empleado) {
		
		Empresa empr1 = empresadao.findById(empleado.getIdEmpresa()).orElse(null);
		Empleado emple1 = new Empleado();
		
		JSONObject obj = new JSONObject();
		
		if(empr1 != null) {
			
			emple1.setEmpresa(empr1);
			emple1.setNombre(empleado.getEmpleado().getNombre());
			emple1.setEdad(empleado.getEmpleado().getEdad());
			emple1.setSueldo(empleado.getEmpleado().getSueldo());
			emple1.setAntiguedad(empleado.getEmpleado().getAntiguedad());
			emple1.setPatrimonio(empleado.getEmpleado().getAntiguedad(), empleado.getEmpleado().getSueldo());
			
			Empleado emple = empleadodao.save(emple1);
			
			obj.put("error", 0);
			obj.put("id del empleado", emple.getId());
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			obj.put("error", 1);
			obj.put("message", "NO SE ENCONTRO LA EMPRESA POR ID");
			
			return ResponseEntity.ok().body(obj.toString());
		}
	}
	
	//creo y envio un ModelAndView con info de la empresa
	@GetMapping("/ver/{id}")
	public ModelAndView verEmpresa(@PathVariable("id") Long id) {
		
		Empresa emp1 = empresadao.findById(id).orElse(null);
		
		ModelAndView mav = new ModelAndView("viewPage");
		
		if(emp1 != null) {
			
			mav.addObject("nombre", emp1.getNombre());
			mav.addObject("direccion", emp1.getDireccion());
			mav.addObject("imagen", emp1.getImagen());
			mav.addObject("id", emp1.getId());
			
		} else {
			
			mav.addObject("nombre", "no se encontro nombre");
			mav.addObject("direccion", "no se encontro direccion");
			mav.addObject("imagen", "no hay imagen");
			mav.addObject("id", "no hay id");
		}
		
	    return mav;
	}
	
	//creo y envio un ModelAndView con los empleados de una empresa, dado un id
	@GetMapping("/ver/{id}/empleados")
	public ModelAndView verEmpleados(@PathVariable("id") Long id) {
		
		Empresa emp1 = empresadao.findById(id).orElse(null);
		
		ModelAndView mav = new ModelAndView("viewEmpleados");
		
		if (emp1 != null) {
			
			List<Empleado> empleadosFound = empleadodao.findByEmpresa(emp1);
			
			if(empleadosFound.size()>0) {
			
				for(Empleado empe : empleadosFound) {
				
					mav.addObject("nombre", empe.getNombre());
					mav.addObject("edad", empe.getEdad());
					mav.addObject("sueldo", empe.getSueldo());
				}
			
			} else {
			
				mav.addObject("nombre", " ");
				mav.addObject("edad", " ");
				mav.addObject("sueldo", " ");
			
			}
			
			mav.addObject("error", 0);
			mav.addObject("empresa", emp1.getNombre());
			mav.addObject("empleados", empleadosFound);
			
			return mav;
			
		} else {
			
			mav.addObject("empresa", "te menti, no hay empresa");
			mav.addObject("error", 1);
			
			return mav;
			
		}
		
	}	
	
	//muestra una sola empresa, por id
		@GetMapping(path= {"/{id}"})
		public ResponseEntity<Object> mostrarEmpresa(@PathVariable("id") Long id) {
			
			Empresa emp1 = empresadao.findById(id).orElse(null);
			
			if(emp1 != null) {
				
				JSONObject obj = new JSONObject();
				
				obj.put("error", 0);
				obj.put("nombre", emp1.getNombre());
				obj.put("direccion", emp1.getDireccion());
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				JSONObject obj = new JSONObject();
				
				obj.put("error", 1);
				obj.put("message", "EL ID NO PERTENECE A UNA EMPRESA");
				
				return ResponseEntity.ok().body(obj.toString());
				
			}
		}
	
	//muestro todas las empresas
		@GetMapping(path= {"/empresas"})
		public ResponseEntity<Object> listarEmpresas() {
				
			List<Empresa> empresasFound = empresadao.findAll();
			JSONArray json_array = new JSONArray();
				
			if(empresasFound.size()>0) {
					
				for (Empresa empre : empresasFound) {
						
					JSONObject aux = new JSONObject();
					aux.put("id", empre.getId());
					aux.put("nombre", empre.getNombre());
					aux.put("direccion", empre.getDireccion());
					json_array.put(aux);
				}
					
				JSONObject obj = new JSONObject();
				obj.put("error", 0);
				obj.put("results", json_array);
					
				return ResponseEntity.ok().body(obj.toString());
					
			} else {
					
				JSONObject obj = new JSONObject();
				obj.put("error", 1);
				obj.put("message", "NO SE ENCONTRARON EMPRESAS");
					
				return ResponseEntity.ok().body(obj.toString());
			}
				
				
				
		}
		
		//muestro los empleados de una empresa, dado su id
		@GetMapping(path= {"/{id}/empleados"})
		public ResponseEntity<Object> listarEmpleados(@PathVariable("id") Long id){
			
			Empresa emp1 = empresadao.findById(id).orElse(null);
			
			if (emp1 != null) {
				
				List<Empleado> empleadosFound = empleadodao.findByEmpresa(emp1);
				
				JSONArray json_array = new JSONArray();
				
				if(empleadosFound.size()>0) {
					
					for(Empleado empe : empleadosFound) {
						
						JSONObject aux = new JSONObject();
						
						aux.put("nombre", empe.getNombre());
						
						json_array.put(aux);
					}
					
					
				} else {
					
					
					JSONObject objerror = new JSONObject();
					objerror.put("error", 1);
					objerror.put("results", "NO HAY EMPLEADOS");
					
					return ResponseEntity.ok().body(objerror.toString());
				}
				
				JSONObject obj = new JSONObject();
				obj.put("error", 0);
				obj.put("nombre", emp1.getNombre());
				obj.put("direccion", emp1.getDireccion());
				obj.put("idEmpresa", emp1.getId());
				obj.put("empleados", json_array);
				
				return ResponseEntity.ok().body(obj.toString());
				
			}
			
				else {
				
				JSONObject objerror = new JSONObject();
				objerror.put("error", 1);
				objerror.put("message", "NO SE ENCONTRO LA EMPRESA");
				
				return ResponseEntity.ok().body(objerror.toString());
			}
		}
		
		//eliminar empresa version JSON
		@GetMapping(path = {"/destruir/{id}"})
		public ResponseEntity<Object> destruir(@PathVariable("id") long id,HttpServletRequest request) {
			
			Principal principal = request.getUserPrincipal();
			
			Empresa empresa = empresadao.findById(id).orElse(null);
			JSONObject objerror = new JSONObject();
			
			if(principal.getName().toString().equals("ignacio")) {

				if(empresa!=null) {
					
					empresadao.deleteById(id);
					
					objerror.put("error", 0);
					objerror.put("message", "Empresa destruida, ud es muy cruel señor ignacio");

					} else {
						
						objerror.put("error", 1);
						objerror.put("message", "no existe esa empresa");
					}
				
			} else {
				
				objerror.put("error", 1);
				objerror.put("message", "ud no poseee los derechos necesarios");
			}

			return ResponseEntity.ok().body(objerror.toString());

		}
		
		//eliminar empresa version MAV
		@GetMapping(path = {"/destroy/{id}"})
		public ModelAndView eliminar(@PathVariable("id") Long id,HttpServletRequest request) {
			
			Principal principal = request.getUserPrincipal();
			
			Empresa empresa = empresadao.findById(id).orElse(null);
			ModelAndView mav = new ModelAndView("delete");
			
			if(principal.getName().toString().equals("ignacio")) {

				if(empresa!=null) {
					
					empresadao.deleteById(id);
					
					mav.addObject("empresa", empresa.getNombre());

					} else {
						
						mav.addObject("empresa", "null");
					}
				
					mav.addObject("usuario", principal.getName());
				
			} else {
				
				mav.addObject("usuario", "user");
				mav.addObject("permisos", "ud no posee los permisos necesarios");
			}
			
			return mav;
			
		}
		
}
