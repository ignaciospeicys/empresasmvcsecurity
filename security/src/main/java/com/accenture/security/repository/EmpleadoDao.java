package com.accenture.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.security.model.Empleado;
import com.accenture.security.model.Empresa;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {
	
	public List<Empleado> findByEmpresa(Empresa empresa);

}
