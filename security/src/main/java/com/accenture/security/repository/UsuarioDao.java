package com.accenture.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.security.model.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, Long> {
	
	Usuario findByUsername(String username);

}
