package com.accenture.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.security.model.Empresa;

public interface EmpresaDao extends JpaRepository<Empresa, Long> {

}
