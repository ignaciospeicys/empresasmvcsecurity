package com.accenture.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.accenture.security.model.Usuario;
import com.accenture.security.repository.UsuarioDao;

@Service
public class MyUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioDao repo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = repo.findByUsername(username);
		if(usuario == null) {
			throw new UsernameNotFoundException("El usuario no existe");
		}
		
		return new UsuarioPrincipal(usuario);
	}

}
